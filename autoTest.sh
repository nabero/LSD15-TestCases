#!/bin/bash


GP_MACHINE_JAR_PATH="./gpmachine.jar"
COMPILER_PATH="/home/$USER/src/compilo/lsd15/src/compiler"
#COMPILER_PATH=""
TMP_DIR="/tmp/lsdtmp"

function breaking()
{

    case "$1" in
	"true")
	    echo "1" > "$TMP_DIR/.breaking"  
	    ;;
	"false")
	    echo "0" > "$TMP_DIR/.breaking"
	    ;;
	*) 
	    cat "$TMP_DIR/.breaking"
	    ;;
    esac
    
}

function test_results()
{
    RES=$1
    PROD_TEST_FILE=$2
    ORIG_TEST_FILE=$3
    PRINT_FILE=$4
    NAME_FILE=$5

    RES_PROD_TEST_FILE=$(cat "$PROD_TEST_FILE")
    RES_ORIG_TEST_FILE=$(cat "$ORIG_TEST_FILE")
    if [[ "$RES_PROD_TEST_FILE" == "$RES_ORIG_TEST_FILE" ]] && [[ $RES -eq 0 ]];
    then
	echo "$NAME_FILE : OK"
    else
	echo "$NAME_FILE : KO, \"$RES_PROD_TEST_FILE\" == \"$RES_ORIG_TEST_FILE\" RES=$RES"
	if [[ "$BREAK_ON_FAULTY" -eq "1" ]];
	then
	    echo "Failure: stopping tests."
	    breaking true > /dev/null

	    if [[ "$CAT_OUTPUT_ON_FAULTY" -eq "1" ]] && [ "$PRINT_FILE" != "" ];
	    then
		echo ""
		echo "Output of $PRINT_FILE :"
		echo "--------------------------------------------"
		tail -10 "$PRINT_FILE"
	    fi
	    
	    echo "$COMPILER_PATH/$COMPILER_NAME < $NAME_FILE"
	    return 
	fi
    fi
}



function make_p_test()
{
    CODE_FILE="$1"
    OUTPUT_FILE="$2"
    INPUT_FILE="$3"
    PROD_OUTPUT_FILE="$4"
    PROD_PCODE_FILE="$5"
    STATUS_FILE=/dev/null

    $COMPILER_PATH/$COMPILER_NAME 2>"$STATUS_FILE" 1>"$PROD_PCODE_FILE" <"$CODE_FILE"
    if [[ "$?" != "0" ]]
    then
	echo -42
    fi
    
    $GP_MACHINE_CMD $PROD_PCODE_FILE <"$INPUT_FILE" 1>"$PROD_OUTPUT_FILE"
    if [[ "$?" != "0" ]]
    then
	echo -21
    fi
}

function make_l_test()
{
    CODE_FILE="$1"
    STATUS_FILE="$2"
    PCODE_FILE="$3"

    $COMPILER_PATH/$COMPILER_NAME 2>"$STATUS_FILE" 1>"$PCODE_FILE" <"$CODE_FILE"
    if [[ "$?" != "0" ]]
    then
	echo -42
    fi

}


function make_tests() 
{
    SINGLE_DIR=$1

    for i in $SINGLE_DIR/*.input;
    do
	INPUT_FILE=${i}
	OUTPUT_FILE=${i%input}output
	PCODE_FILE=${i%input}pc
	CODE_FILE=${i%input}code
	STATUS_FILE=${i%input}status

	PROD_PCODE_FILE=$TMP_DIR${PCODE_FILE#$TEST_PATH}
	PROD_OUTPUT_FILE=$TMP_DIR${OUTPUT_FILE#$TEST_PATH}

	CURRENT_TMP_DIR=$TMP_DIR${SINGLE_DIR#$TEST_PATH}

	MODE=unknown

	if [[ -f "$CODE_FILE" ]]
	then
	    # if code file provided, assuming P mode
	    if [[ -f "$INPUT_FILE" ]] && [[ -f "$OUTPUT_FILE" ]]
	    then
		MODE=P
	    fi
	else
	    # if 
	    if [[ -f "$INPUT_FILE" ]] && [[  -f "$OUTPUT_FILE" ]]
	    then
		MODE=L
	    fi
	fi

	ensure_tmpdir $CURRENT_TMP_DIR

	if [[ "$MODE" == "P" ]]
	then
	    if [[ "$GP_MACHINE_CMD" == "-1" ]]
	    then
		continue
	    fi
	    if [[ "$PREPEND_CMD" == "1" ]]
	    then
		echo testing "$CODE_FILE"
	    fi
	    RES=$(make_p_test "$CODE_FILE" "$OUTPUT_FILE" "$INPUT_FILE" "$PROD_OUTPUT_FILE" "$PROD_PCODE_FILE")
	    test_results "$RES" "$OUTPUT_FILE" "$PROD_OUTPUT_FILE" "$PROD_PCODE_FILE" "$CODE_FILE"
	    
	elif [[ "$MODE" == "L" ]]
	then
	    if [[ "$PREPEND_CMD" == "1" ]]
	    then
		echo testing "$INPUT_FILE"
	    fi
	    RES=$(make_l_test "$INPUT_FILE" "$PROD_OUTPUT_FILE" "$PROD_PCODE_FILE")
	    test_results "$RES" "$OUTPUT_FILE" "$PROD_OUTPUT_FILE" "$PROD_PCODE_FILE" "$INPUT_FILE"
	fi



	if [[ "$(breaking)" = "1" ]]
	then
	    return
	fi
    done
}


function make_all_tests() 
{
    BASE_PATTERN=""
    if [[ "$SET_SERIE" ]] && [[ "$SET_LEVEL" ]] 
    then
	BASE_PATTERN="$SET_SERIE$SET_LEVEL"
    elif [[ "$SET_SERIE" ]]
    then
	BASE_PATTERN="$SET_SERIE"
    elif [[ "$SET_LEVEL" ]] 
    then
	BASE_PATTERN="[LP]$SET_LEVEL"
    fi

    for i in $TEST_PATH/$BASE_PATTERN*;
    do
	if [[ -d "$i" ]];
	then
	    make_tests "$i"
	    if [[ "$(breaking)" -eq "1" ]];
	    then
		return
	    fi
	fi
    done
}



function ensure_tmpdir() 
{
    SERIE=${1#$TMP_DIR}

    if [[ ! -d "$TMP_DIR" ]];
    then 
	mkdir "$TMP_DIR"
	#echo "TMP dir isn't available, please create it first : $TMP_DIR"
	#exit 1
    fi

    if [[ ! -d ${TMP_DIR}/${SERIE} ]]
    then
	mkdir -p ${TMP_DIR}/${SERIE}
    fi

}

ensure_tmpdir


SET_SERIE=""
for i in `seq 0 $#`
do
    if [[ "$1" == "" ]]
    then
	continue
    elif [[ $1 == -s ]]
    then
	shift
	SET_SERIE=$1
    elif [[ $1 == -l ]]
    then
	shift
	SET_LEVEL=$1
    elif [[ $1 == -p ]]
    then
	PREPEND_CMD=1
    else
	COMPILER_PATH=$1
    fi
    shift
done


if [[ ! "$1" ]] && [[ ! "$COMPILER_PATH" ]];
then
    echo "usage: $0 <compiler_path>"
    echo "This script assumes that the Makefile to create the compiler is in the same directory as the compiler"
    echo ""
    echo "The following parameters can be provided :"
    echo "  -s    Chose one serie (L|P) to run. Both is the default value."
    echo "        All the L test are done before all the P tests in current implementation."
    echo "        This may change in the future.."
    echo "  -l    Chose one or multiple level to run"
    echo "        Ex: -l 1  => run level 1 only for L & P series"
    echo "            -l [1-3] => run level 1 to 3 "
    echo "  Those parameters can be combined to select directories corresponding"
    echo "  to pattern <testDir/\$S_SWITCH\$L_SWITCH*> "
    echo
    echo "NOTE: the header of this file can be edited to 'hardcode' some of the values,"
    echo "      and examples are provided !"
    exit 1
fi


COMPILER_NAME=$(basename $COMPILER_PATH)
COMPILER_PATH=`cd ${COMPILER_PATH%$COMPILER_NAME}; pwd`
TEST_PATH=$(pwd)

echo "Serie : $SET_SERIE"
echo "Level : $SET_LEVEL"
echo "Compiler : $COMPILER_PATH"

AUTOBUILD=1
BREAK_ON_FAULTY=1
CAT_OUTPUT_ON_FAULTY=1

eval COMPILER_PATH=$COMPILER_PATH
eval TEST_PATH=$TEST_PATH


cd "$COMPILER_PATH"

# GP_MACHINE_CMD Setter

if [[ -f "$TEST_PATH/$GP_MACHINE_JAR_PATH" ]]
then
    GP_MACHINE_CMD="java -jar $TEST_PATH/$GP_MACHINE_JAR_PATH -nogui -n"
elif [[ -f "$GP_MACHINE_JAR_PATH" ]]
then
    GP_MACHINE_CMD="java -jar '$GP_MACHINE_JAR_PATH' -nogui -n"
else
    echo "WARNING: gpmachine could not be found !"
    echo "         Px test will be skipped".
    echo "         Please add gpmachine.jar in this directory."    
    sleep 5
    GP_MACHINE_CMD=-1
fi

cd "$COMPILER_PATH"

while [[ $AUTOBUILD ]]; 
do 

    make clean &> /dev/null 
    make 
    
    if [[ "$?" -eq "0" ]];
    then
	$(breaking false)
	make_all_tests
    else
	echo "Could not build..."
    fi
    inotifywait -s -e modify *.l *.y *.h *.c 
    reset
done
